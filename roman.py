ROMAN_NUMERALS = {
    "I": 1,
    "V": 5,
    "X": 10,
    "L": 50,
    "C": 100,
    "D": 500,
    "M": 1000,
}

INVALID_TOKEN = "INVALID_TOKEN"
INVALID_TOKEN_ORDER = "INVALID_TOKEN_ORDER"


def validate_roman_section_syntax(input_text):
    return all(sym in ROMAN_NUMERALS for sym in input_text)


def translate_roman_numerals_to_numbers(roman_numerals_section):
    results = []
    previous_number = 0

    for numeral in roman_numerals_section:
        current_number = ROMAN_NUMERALS[numeral]

        if len(results) > 0 and ROMAN_NUMERALS[numeral] > previous_number:
            current_number = ROMAN_NUMERALS[numeral] - previous_number
            results.pop()

        results.append(current_number)
        previous_number = current_number

    return results


def validate_section_results(section_results):
    for i, result in enumerate(section_results):
        if i > 0:
            if section_results[i] > section_results[i - 1]:
                return False
    return True


def process_roman_section(section):
    if not validate_roman_section_syntax(section):
        return INVALID_TOKEN

    else:
        numbers = translate_roman_numerals_to_numbers(section)

        if not validate_section_results(numbers):
            return INVALID_TOKEN_ORDER

        else:
            return sum(numbers)
