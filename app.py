import sys

from collections import defaultdict

from roman import (
    process_roman_section
)

from european import (
    process_european_section
)


ARGUMENT_ERROR = "ARGUMENT_MISSING"


def is_european(section):
    if section.isdigit():
        return True


def process_input(full_input):
    final_result = defaultdict(str)

    for section in full_input:
        if is_european(section):
            final_result[section] = process_european_section(section)
        else:
            section = section.upper()
            final_result[section] = process_roman_section(section)

    return final_result


def print_final_results(results):
    for numerals, result in results.items():
        print("{} = {}".format(numerals, result))


def collect_arguments(args):
    args.pop(0)
    return args


def main():
    args = collect_arguments(sys.argv)
    if len(args) > 0:
        print_final_results(process_input(args))
    else:
        print(ARGUMENT_ERROR)


if __name__ == "__main__":
    main()
