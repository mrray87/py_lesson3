EURO_MAX = 3999

EXCEEDS_BOUNDARY = "NUMBER_EXCEEDS_BOUNDARY"


def verify_boundary(section):
    return 0 < int(section) <= EURO_MAX


def process_european_section(section):
    if verify_boundary(section):
        return "VALID_EUROPEAN_SECTION"
    else:
        return EXCEEDS_BOUNDARY
